from figuras import (Cuadrado, PoligonoRegular)
import math
from rich import print

"""
Encontrar el heptágono 
cuya área sea tres veces 
la de un pentágono de lado 3. 
Retornar sus lados.

heptagono = 7 lados
pentagono = 5 lados
"""

nro_lados= 5

longitud_lado = 3

pentagono = PoligonoRegular(longitud_lado, nro_lados)

area_pentagono = pentagono.area()
print("Area de pentagono",area_pentagono)

 
heptagono_1 = PoligonoRegular(longitud_lado, 7)
print(heptagono_1)

print(heptagono_1.to_dict())

print(f'La relación de un pentagóno en comparación con un heptagono')
print(heptagono_1.area() / area_pentagono)

inicio = longitud_lado 

delta = .0001

print("Aumenta el tamaño del lado con una iteración")

while True:
	heptagono = PoligonoRegular(inicio,7)
	if heptagono.area() >= 3 * area_pentagono: 
		print(heptagono.to_dict())
		break
	else:
		inicio += delta

