from figuras import (Circulo, PoligonoRegular)
import math
from rich import print

radio = 100
circulo = Circulo(radio)

n = 3
delta = 1

# calcular angulo 
def angulo(n):
    if n>0:
        return 2*math.pi / n
    else:
        return 0

gamma = angulo(n)
print("GAMMA",gamma)

lado = 2 * radio * math.sin(gamma/2)
print("LADO",lado)
poligon = PoligonoRegular(lado,n)
print("POLIGON",poligon)

area_p = poligon.area()
print("AREA POLIGONO",area_p)

area_c = circulo.area()
print("AREA CIRCULO:", area_c)


while True:
	poligon = PoligonoRegular(lado,n)
	if poligon.area() >= 99.99 * (area_c /100):
		print("La relación es:", poligon.to_dict())
		break
	else:
		n += delta
		gamma = angulo(n)
		lado = 2 * radio * math.sin(gamma/2)

