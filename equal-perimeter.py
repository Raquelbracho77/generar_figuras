from figuras import (Circulo, Triangulo, 
	Cuadrado, PoligonoRegular)
import math
from rich import print
"""
Encontrar un:
- tríangulo equilátero, 
- un cuadrado
- un hexagono 
- un heptágono 
que tengan el mismo perímetro, 
 > retornar el lado de cada figura.
"""
# perimetro = input("defina perimetro ")
figuras = {'triangulo equilatero': 3, 'cuadrado': 4, 'hexagono': 6, 'heptagono':7}

perimetro = 10

for llave,valor in figuras.items():
	print(llave,valor)
	lado = perimetro / valor
	print("longitud del lado",lado)
	figura = PoligonoRegular(lado,valor)
	print(figura.to_dict())


