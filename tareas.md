Ejercicios usando Figuras
===================


I) En otro proyecto, una vez instalada 'figuras' como módulo

```bash
pip install -e .
```

Realizar lo siguiente:

- script que pregunte nombre de figura
- pregunte parámetros
- calcular área y perímetro
- crear diccionario con sus valores (de entrada  y resultados)
- acumular en una lista
- terminar con END o FIN
- Guardar lista como json en un archivo .json
- mostrar la lista de diccionarios (imprimir)

Modulos recomendados para usar

- json
- pprint
- rich
- click
- math

Para crear los diccionarios se recomienda:

- implementar método to_dict en Figura base
- implementar método to_dict en cada clase Figura, haciendo al inicio 'super()' para
rescatar lo que hace la clase base.
- cada diccionario debe tener los campos de entrada y calculados, todos los usados

II) Calcular y comparar figuras.

- [x] Encontrar un tríangulo cuya área sea igual al área de un octágono.
- [x] Encontrar un cuadrado cuyo perímetro se tres veces el perímetro de un dodecágono. ¿Qué area tendrían ambos? 
- [x] Encontrar un tríangulo equilátero, un cuadrado, un hexagono y un heptágono que tengan el mismo perímetro, retornar el lado de cada figura.
- [x] Encontrar el heptágono cuya área sea tres veces la de un pentágono de lado 3. Retornar sus lados.
- [] Encontrar la relación entre un poligono regular circunscrito en un circulo de radio R, tal que el area del poligono sea un 99.99% igual a la del circulo. Encontrar el N de lados que satisfaga la relación


¿Cómo resolver?
 Este ejercicio consiste en comparar figuras, la técnica o algoritmo que se puede ocupar es utilizar valores incrementales de manera secuencial. Además de implementar
 algún método abstracto en cada clase que permita retornar los parámetros base a partir del área o del perímetro. Así podemos observar que es posible obtener algunas cosas de manera inversa.
 
 
 
 











