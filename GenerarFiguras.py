from figuras import (Circulo, Triangulo, 
	Cuadrado, PoligonoRegular)
# Rich
from rich import print
from typing import List
from rich.console import Console, OverflowMethod
from rich.layout import Layout

# ----
from collections import OrderedDict as od


def is_numeric(valor):
	if valor.isdigit():
		return True

	else:
		try:
			float(valor)
			return True
		except Exception:
			return False

	return False



opciones = {
	'🔴 circulo':{
		'clase':Circulo,
		'input':od({'radio':float}),
	},
	'▲ triangulo':{
		'clase':Triangulo,
		'input':od([(k,float) for k in  ('a', 'b', 'c')]), # lados del triangulo
	},
	'⬜ cuadrado':{
		'clase':Cuadrado,
		'input':od({'a':float})
	},
	'⭓ poligono regular':{
		'clase':PoligonoRegular,
		'input':od([('a',float), ('cant_lados',int)])
	}
}

tipos={i : name for i,name in enumerate(opciones) }

figuras = []
finalizar = {
	'end', 'fin', 'ok',
}	
while True:
	try: 
		# rich
		console = Console(width=55)
		style = "bold white on #8f0005"
		info_style = "bold #87cefa"

		console.print(" 🐍 Bienvenido al módulo de geometría hecho en python," , style=style, justify="left")
		console.print("Éstas son las ✨opciones:✨", style=style, justify="left")
		console.print(" ℹ️ Información", style=info_style)
		console.print(f" 🐍 Para detener el módulo y crear un archivo .json, ingresa: end, ok, fin", style="italic")
		for i,tipo in tipos.items():
			print(f'[#8f0005]{i}[/#8f0005]',f'[yellow]{tipo}[/yellow]')

		id_figura = input(" 🐍 Selecciona una figura: ")
		if id_figura.lower() in finalizar:
			break

		# if id_figura in opciones:
		if id_figura.isdigit():
			figura_name = tipos.get(int(id_figura))
			print(f' 🐍 Selecionaste un: [bold][yellow]{figura_name}[/yellow][/bold]')
			figura = opciones.get(figura_name)

		else:
			figura = opciones.get(id_figura) 
			print(figura)

		if figura:
			clase = figura.get('clase')
			argumentos = figura.get('input')
			console.print(f" 🐍 [bold]La figura es la número:[/bold] [bold][yellow]{clase}[/yellow][/bold]")
			console.print(f" 🐍 Ahora, ingresa los 'argumentos' en forma numérica para cada elemento:", style="i #ffa500")
			print(argumentos)
			valores = input(" 🐍 Debes separar los valores con una ',' si tiene más de un argumento: ").split(',')
			console.print(valores, style="bold #ffa500")
			verifica = list(map(is_numeric, valores))
			if all(verifica) and  len(verifica) == len(argumentos):
				valores_dict = {}
				for i,name in enumerate(argumentos.keys()):
					conversion = argumentos.get(name)
					valor = conversion(valores[i]) # La conversion será int o float segun la fn is_numeric
					valores_dict[name]=valor 

				console.print(" 🐍 Estos son los valores que van a generar la figura", style= style)
				print(valores_dict)
				figura = clase(**valores_dict)
				figuras.append(figura.to_dict())
				console.print(f"Figura creada: ",figura)
				print(figura.to_dict())
			else:
				print("Valores ingresados de manera incorrecta")
		else:
			print(f"No existe {id_figura} ingresado")
	except KeyboardInterrupt as k:
		print("Cerrando el modulo")
		raise

# guardar en .json
import json
guardar = 'figuras.json'
with open(guardar, 'w') as f:
	json.dump(figuras,f,indent = 2)
	console.print(f"🐍  Se creó el archivo: {guardar}", style="bold #66cdaa")
		# print([f'{i} : {name}' for i,name in enumerate(opciones) ])