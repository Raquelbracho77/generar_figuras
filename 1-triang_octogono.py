from figuras import Triangulo, PoligonoRegular
from cantidad_triangulos import cant_triangulos
import math
from rich import print

 
n = 8
lado = 10
poligono = PoligonoRegular(lado,n)

print(poligono.to_dict())
area_oct = poligono.to_dict().get("area")
def angulo(n):
    if n>0:
        return 2*math.pi / n
    else:
        return 0

gamma = angulo(n)       

def lado_isosceles(lado, gamma):
    return (lado / 2) *  1 / math.sin( gamma  / 2)

def area_subtriangulo_poligono(lado, gamma):
    return math.pow(l/2, 2) * 1 / math.tan(gamma / 2)


"""
usamos automáticamente la cantidad de bases necesarias.
"""

# cant triangulos -> cantidad de lados del poligono.
# busar el N de base_max para limitar nuestra iteracion
# dado N, tenemos base_min = N - 1

N = 1
while True:
    ct = cant_triangulos(N)
    print("base N =", N, "# triangulos",  ct)
    if ct >= n:
        break
    N += 1

print("Valor N base max", N)
base_min = (N-1) * lado
base_max = N * lado

print("Cantidad de triangulos")

delta = 0.1

base = base_min
print("Iniciando iteracion")

flag = True
while flag:
    while base < base_max:
        iso_lado = lado_isosceles(base, gamma)
        triangulo = Triangulo(iso_lado, base, iso_lado)
        if triangulo == poligono:
            print(triangulo.to_dict())
            flag = False
            break
        base += delta

    if flag:
        delta /= 10
        base = base_min
        print(f"Reduce delta {delta}")
    else:
        break


