def cant_triangulos(n_base):
    if n_base == 1:
        return 1
    else:
        return cant_triangulos(n_base-1) + 2 * (n_base - 1) + 1

if __name__ == "__main__":
    print(cant_triangulos(1))
    print(cant_triangulos(2))
    print(cant_triangulos(3))
    print(cant_triangulos(4))
    print(cant_triangulos(50))
