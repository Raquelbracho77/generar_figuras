from figuras import (Circulo, Triangulo, 
	Cuadrado, PoligonoRegular)
# Rich
from rich import print
from typing import List
from rich.console import Console, OverflowMethod
from rich.layout import Layout
from rich.table import Table

# ----
from collections import OrderedDict as od

import click

def is_numeric(valor):
	if valor.isdigit():
		return True

	else:
		try:
			float(valor)
			return True
		except Exception:
			return False

	return False

opciones = {
	'Circulo':{
		'clase':Circulo,
		'input':od({'radio':float}),
		'icon':'●'
	},
	'Triangulo':{
		'clase':Triangulo,
		'input':od([(k,float) for k in  ('a', 'b', 'c')]), # lados del triangulo
		'icon':'▲'
	},
	'Cuadrado':{
		'clase':Cuadrado,
		'input':od({'a':float}),
		'icon':'▅'
	},
	'Poligono Regular':{
		'clase':PoligonoRegular,
		'input':od([('a',float), ('cant_lados',int)]),
		'icon': '⭓'
	}
}

@click.command()
@click.argument('guardar', default='figuras.json')
def crear_figura(guardar):

	tipos={i : name for i,name in enumerate(opciones) }

	figuras = []
	finalizar = {
		'end', 'fin', 'ok',
	}	

	# Definición de consola general para todo el script
	console = Console()
	# Tablas
	table = Table(title="Figuras Geométricas") # titulo de la tabla
	table.add_column("Figura", justify="right", style="cyan", no_wrap=True)
	table.add_column("Area", style="magenta")
	table.add_column("Perímetro", style="magenta")
	table.add_column("Argumentos", style="white")


	#style = "bold white on #8f0005"
	#info_style = "bold #87cefa"

	while True:
		try: 
			console.print(" 🐍 Bienvenido al módulo de geometría hecho en python" , style="bold white on #8f0005", justify="center")
			# información
			# = Table(Title)
			# console.print(" ℹ️ Información", style="bold #87cefa")
			#console.print(f" 🐍 Para detener el módulo, ingresa: end, ok, fin", style="italic")

			info = "Para detener el módulo, ingresa: end, ok, fin"
			overflow_method: List[OverflowMethod] = ["Información"]
			for overflow in overflow_method:
				console.rule(overflow)
				console.print(info, style="bold cyan", justify="right")
				console.print()

			console.print("Éstas son las ✨opciones:✨",  justify="left")

			i = 0
			for tipo,dato in opciones.items():
				icono = dato['icon']
				print(f'[#8f0005]{i}[/#8f0005]',f'[#8a2be2]{icono}[/#8a2be2]',f'[yellow]{tipo}[/yellow]')
				i+=1

			id_figura = input(" 🐍 Selecciona una figura: ")
			if id_figura.lower() in finalizar:
				break

			# if id_figura in opciones:
			if id_figura.isdigit():
				figura_name = tipos.get(int(id_figura))
				print(f' 🐍 Selecionaste un: [bold][yellow]{figura_name}[/yellow][/bold]')
				figura = opciones.get(figura_name)

			else:
				figura = opciones.get(id_figura) 
				print(figura)

			if figura:
				clase = figura.get('clase')
				argumentos = figura.get('input')
				console.print(f" 🐍 [bold]La figura es la número:[bold][yellow]{clase}[/yellow][/bold]")
				console.print(f" 🐍 Ahora, ingresa los 'argumentos' en forma numérica para cada elemento:", style="i #ffa500")
				
				for llave,valor in argumentos.items():
					print(f'{llave} : tipo:{valor.__name__}')

				valores = input(" 🐍 Debes separar los valores con una ',' si tiene más de un argumento: ").split(',')
				console.print(valores, style="bold #ffa500")
				verifica = list(map(is_numeric, valores))
				if all(verifica) and  len(verifica) == len(argumentos):
					valores_dict = {}
					for i,name in enumerate(argumentos.keys()):
						conversion = argumentos.get(name)
						valor = conversion(valores[i]) # La conversion será int o float segun la fn is_numeric
						valores_dict[name]=valor 

					console.print(" 🐍 Estos son los valores que van a generar la figura", style= "bold white on #8f0005")
					print(valores_dict)
					figura = clase(**valores_dict)
					figuras.append(figura.to_dict())
					console.print(f"Figura creada: ",figura)
					#print(figura.to_dict())
					entrada = ','.join([f'({k},{b})' for k,b in valores_dict.items()])
					diccionario = figura.to_dict()

					area = diccionario.get('area')
					perimetro = diccionario.get('perimetro')
					table.add_row(
						diccionario.get('nombre'),
						f'{area:.4f}',
						f'{perimetro:.4f}',
						entrada)

				else:
					print("Valores ingresados de manera incorrecta")
			else:
				print(f"No existe {id_figura} ingresado")
		except KeyboardInterrupt as k:
			print("Cerrando el modulo")
			raise

	# guardar en .json
	import json
	#guardar = 'figuras.json'
	
	with open(guardar, 'w') as f:
		json.dump(figuras,f,indent = 2)
		console.print(f"🐍  Se creó el archivo:",f"✨ {guardar} ✨", style="bold #66cdaa")
	console.print(table)
			# print([f'{i} : {name}' for i,name in enumerate(opciones) ])


if __name__ == '__main__':
	crear_figura()