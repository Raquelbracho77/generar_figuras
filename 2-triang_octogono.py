from figuras import Triangulo, PoligonoRegular
import math
from rich import print
from rich.console import Console

console = Console()
console.print()
console.print("1. Triangulo con base igual al área de octágono")
# nro de lados 
n = 8
# largo del lado del triang
lado = 10

octogono = PoligonoRegular(lado,n)

print(octogono.to_dict())
area_oct = octogono.to_dict().get("area")
def angulo(n):
    if n>0:
        return 2*math.pi / n
    else:
        return 0

gamma = angulo(n)       

def lado_isosceles(lado, gamma):
    return (lado / 2) *  1 / math.sin( gamma  / 2)

def area_subtriangulo_poligono(lado, gamma):
    return math.pow(l/2, 2) * 1 / math.tan(gamma / 2)

base_min = 2 * lado # 
base_max = 4 * lado

delta = 0.00001

base = base_min
print("Iniciando iteracion")

while base < base_max:
    iso_lado = lado_isosceles(base, gamma)
    triangulo = Triangulo(iso_lado, base, iso_lado)
    if triangulo == octogono:
        print(triangulo.to_dict())
        break
    base += delta





