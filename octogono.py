from figuras import (Circulo, Triangulo, 
	Cuadrado, PoligonoRegular)
import math

# class Octogono(PoligonoRegular):
# 	def __init__(self, lado):
# 		super().__init__(8,lado)

	octogono = PoligonoRegular(8, 10)
	cant_lados = 8

	pi = 3.1416
	gamma = pi/4

	def iso_lado(lado):
		return (lado/2)*(1/math.sin(gamma/2))

	def perimetro(lado):
		return lado * (1 + 1 / math.sin(gamma/2))

	def area(lado):
		a = iso_lado(lado)
		p = perimetro(lado)
		s = p / 2
		return math.sqrt(s * math.pow(( s - a ), 2 ) * ( s - lado ))

	Amin = 	216.214
	Amax = 1107.963

	def lado(area):
		return math.sqrt(area * 8 * math.tan(gamma/2))


	base_min = lado(Amin)
	
	base_max =lado(Amax)

	paso = 0.01 # como epsilon

	valor = base_min

	while valor <= base_max:
	triangulo = Triangulo(valor)
		if triangulo == octogono:
		console.print(triangulo.to_dict())
		break
	valor += paso
