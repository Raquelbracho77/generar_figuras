from figuras import Triangulo, PoligonoRegular
from rich import print
import math

# -- NO -- VA -- ESTE

octogono = PoligonoRegular(8, 10)
# cantidad de lados del octogono
n = 8

gamma = pi/4

def iso_lado(lado):
    return (lado/2)*(1/math.sin(gamma/2))

def perimetro(lado):
    return lado  *  (1 + 1/math.sin(gamma/2) )

def area(lado):
    a = iso_lado(lado)
    p = perimetro(lado)
    s = p / 2
    return math.sqrt(s*math.pow((s-a),2)*(s-lado))


Amin = 216.214
Amax = 1107.963

def lado(area):
    return math.sqrt(area*8*math.tan(gamma/2))


# el lado define todo
# dada un area inicial, tenemos un lado inicial
# calculamos el triangulo mas chico (la base)

b_min = lado(Amin)
# calculamos el triangulo mas grande (la base)

b_max = lado(Amax)

# se debe interar desde b_min hasta b_max

paso = 0.01

valor = b_min

while valor<=b_max:
    triangulo = Triangulo(valor)
    if triangulo == octogono:
        print(triangulo.to_dict())
        break
    valor += paso
    
    
    
#    >>> import click
#>>> from figuras import (Circulo, Triangulo,
#... |┈┈┈Cuadrado, PoligonoRegular)
#>>> from figuras import (Circulo, Triangulo, Cuadrado, PoligonoReg
#... ular)
#>>>
#>>> class Octogono(PoligonoRegular):
#...    def __init__(self, lado):
#...        super().__init__(8,lado)
#>>>
#>>> octogono1=Octogono(10)
#>>> octogono1.to_dict()
#
