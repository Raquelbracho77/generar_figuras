from figuras import (Cuadrado, PoligonoRegular)
import math
from rich import print


"""
Encontrar un cuadrado 
cuyo perímetro sea tres 
veces el perímetro de un 
dodecágono. ¿Qué area tendrían ambos? 
"""
nro_lados = 12

longitud_lado = 5
print("lado is:",type(longitud_lado))

dodecagono = PoligonoRegular(longitud_lado, nro_lados)

print("Calculando perímetro")
perimetro_dod = dodecagono.perimetro()
print("PERIMETRO",perimetro_dod)

longitud_lado = perimetro_dod/4

cuadrado = Cuadrado(longitud_lado)
print(cuadrado.to_dict())